# IOT Basics

## Industrial Revolution  

* **Industry 1.0** - Mechanization, Steam power , weaving Looms.
* **Industry 2.0** - Mass production, Assembly line, electrical energy.
* **Industry 3.0** - Automation, Computers & Electronics to handle factory work.
* **Industry 4.0** - Cyber physical systems, Internet of things(IOT),Network.


![IndusRev](https://www.onupkeep.com/answers/wp-content/uploads/2019/05/Industry4point0-768x512-300x200.png)

Now focusing on Industry 3.0

### Industry 3.0  

Industry 3.0 was a huge leap ahead wherein the advent of computer and automation ruled the industrial scene. It was during this period of transformation era where more and more robots were used in to the processes to perform the tasks which were performed by humans. E.g. Use of Programmable Logic Controllers(PLCs), Robots etc.  

#### Industry 3.0 Hierarchy & Architecture

![gh](https://www.elomatic.com/en/assets/images/utility-pages/expert_articles/industrial_automation_diagram%201.png)  

* **Field Devices** - They are at the bottom of the Hierarchy. These are the sensors or actuators present in the factory, they measuring things or actuate machines (doing some actions). 

* **Control Devies** - These are the PCs , PLCs etc. They tell the field devices what to do and control them.
* **Station** - A set of Field devices & Control Devices together is called a station.
* **Work Centres** - A set of stations together is called a work centre (Factories). They use softwares like **Scada & Historian**.
* **Enterprise** - A set of Work Centres together becomes an Enterprise. They use softwares like **ERP & MES**.  

The softwares used like Scada, Erp etc are used to store data, arrange data given to them by the field devices through the control devices.



### Industry 3.0 Protocols

Protocols are the guidlines used to for sending and receiving data.

Some of the protocols used by Industry 3.0 are
* Modbus 
* Profi-Network
* CANopen
* EtherCAT


 ## Industry 4.0

Basically it is industry 3.0 connected to the internet.

 Now we can remotely access the data recieved because of the industries connected to the internet.


 ### Industry 4.0 Architecture

 
  ![photo](https://image.slidesharecdn.com/opensourceforindustry4-171120165931/95/open-source-software-for-industry-40-6-638.jpg?cb=1511204195)

 

 Now the data recieved is sent to the cloud.

 Some of the protocols used by the industry 4.0 are

  * MQTT
  * Web Sockets
  * https
  * REST API
   
  All these protocols are optimized to send data to the cloud for data analysis

![protocols](https://data-flair.training/blogs/wp-content/uploads/sites/2/2018/06/IoT-Protocol.jpg) 

### What we can do with Industry 4.0 

 ![photo](https://lucidworks.com/wp-content/uploads/2019/05/Industry-4p0-Blog-Header-1658x468.png)


 ## Comparing Industry 3.0 to Industry 4.0

  ![photo](https://techinsight.com.vn/wp-content/uploads/2018/07/4-1.png)  


  #### Problems Faced with Industry 4.0 upgradation

  * Cost.
  * Downtime.
  * Reliablity.

  #### Challenges Faced in Conversion From 3.0 to 4.0

  * Expensive hardware
  * Lack of Documentation
  * Properitary PLC prtocol 

### HOW TO CONVERT FROM 3.0 to 4.0 
 
 We can use a library called Shunya Interfaces. It can used on an embedded system like raspberry pi and use it as per our requirements.

![photo](https://gitlab.com/ganesh007/iotmodule1/-/raw/2b76b6d4c8f93de7105907e401ef5a76b0f21d7d/extras/1.png) 

 Step 1: Identifying the most popular Industry 3.0 devices.

 Step 2: Study Protocols that these devices communicate.

 Step 3: Get data From the industry 3.0 devices.

 Step 4: Send the data to the cloud for industry 4.0


  After the data is sent to the internet/cloud, we can

  1. Store data in **Time Shared Data database(TSDB)** using tools like

  * Prometheus
  * InfluxDB
  
  2. Make a **Dashboard** of the datas recieved using tools like

  * Grafana 
  * Thingsboard

  3. **Analyse** Data using platforms like

  * AWS IoT
  * Google IoT
  * Azure IoT
  * Thingsboard  

  ![iot platforms](https://devopedia.org/images/article/86/2438.1528650763.jpg)

  4. Gets **Alerts** using platforms like 

  * Zaiper
  * Twilio 

  This how the data inside the cloud looks like 

   ![photo](https://gitlab.com/ganesh007/iotmodule1/-/raw/2b76b6d4c8f93de7105907e401ef5a76b0f21d7d/extras/2.png)

